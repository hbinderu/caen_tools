Hello there.

1. Copy the raw CAEN log files into the folder 'caen_logs_unformatted'.

2. Create an Alias list with the same format as the example found in Aliases folder. The Alias file is connected to the corresponding caen_log file. Make sure that they have the same filename (Alias: .csv , caen_log: .txt) Save the lists in the Aliases folder. Make sure that the BoardID is corresponding to the name configured in the CAEN server.

3. Copy the radmon rapport into the TID folder. In the script 'CAEN_tools.py' there are options to change what coloums containing what kind of information. Make sure that both TID and HEH is included.
Go to the format_radmon.py script and change the timestamps and values. These are the inital values of each week - or test cycle.

4. OPTIONAL: 	Include a gradient file like the example in 'gradient' folder. 
		There is also an option in 'CAEN_tools.py' to enable "inital currents". These values are hardcoded and will require some more tweaking. Go to the corresponding function from CAEN_tools.py.

5. Run 'CAEN_tools.py'

import csv
import os
import time
from datetime import datetime


class RadmonFormatter:
    def __init__(self):
        self.random_dir = 'TID/radmon.csv'

        # TID doses when the beam was turned off during each week
        self.TID_doses = [588.4966032, 1013.597506]

        # HEH doses when the beam was turned off during each week
        self.HEH_doses = [2259520000000, 3891690000000]

        # Timestamps when the beam was turned off during each week
        self.timestamp1 = self.timestamp_parser('08/08/2018 15:05:25')
        self.timestamp2 = self.timestamp_parser('15/08/2018 17:15:05')
        self.timestamps = [self.timestamp1, self.timestamp2]

    # Convert timestamp string to datetime object
    def timestamp_parser(self, timestamp):
        return datetime.strptime(timestamp, '%d/%m/%Y %H:%M:%S')

    # Will subtract the defined doses after the specified timestamps
    def run_format(self, tid_index=5, heh_index=4, ts_index=0, gy_to_tid=True):
        gy_factor = 10 if gy_to_tid else 1

        with open(self.random_dir, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # Skip header

            # As we're appending to the file, we want to remove it so there's not multiple data
            if os.path.exists('TID/TID.csv'):
                os.remove('TID/TID.csv')

            with open('TID/TID.csv', 'ab') as fd:
                writer = csv.writer(fd)
                writer.writerow(['Timestamp', 'TID', 'HEH'])    # write header
                for row in reader:
                    try:
                        reader_ts = self.timestamp_parser(str(row[ts_index]))
                        last_timestamp_index = len(self.timestamps) - 1

                        # Finding the correct interval defined by timestamps and adjust the dose
                        for index, ts in enumerate(self.timestamps):
                            # Later than the last timestamp
                            if reader_ts >= self.timestamps[last_timestamp_index]:
                                data_tid = (float(row[tid_index]) - self.TID_doses[last_timestamp_index]) / gy_factor
                                data_heh = float(row[heh_index]) - self.HEH_doses[last_timestamp_index]
                                writer.writerow([row[ts_index], str(data_tid), str(data_heh)])
                                break
                            # Between two timestamps
                            elif ts <= reader_ts < self.timestamps[index + 1]:
                                data_tid = (float(row[tid_index]) - self.TID_doses[index]) / gy_factor
                                data_heh = float(row[heh_index]) - self.HEH_doses[index]
                                writer.writerow([row[ts_index], str(data_tid), str(data_heh)])
                                break
                            # Before the first timestamp
                            elif reader_ts < self.timestamps[0]:
                                writer.writerow([row[ts_index], str(float(row[tid_index]) / gy_factor), str(row[heh_index])])
                                break
                    except ValueError:
                        pass

    # Won't changed the files values except from setting it up for plotting format and changing Gy to TID if wished
    def run_unformatted(self, tid_index=5, heh_index=4, ts_index=0, gy_to_tid=True):
        gy_factor = 10 if gy_to_tid else 1

        with open(self.random_dir, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            next(reader)    # Skip header

            # Delete file if it exists, as we'll append to the file (remove multiple occurrences)
            if os.path.exists('TID/TID_unformatted.csv'):
                os.remove('TID/TID_unformatted.csv')

            # Open .csv file and append binary to the file
            with open('TID/TID_unformatted.csv', 'ab') as fd:
                writer = csv.writer(fd)
                writer.writerow(['Timestamp', 'TID', 'HEH'])    # write header
                for row in reader:
                    writer.writerow([row[ts_index], str(float(row[tid_index]) / gy_factor), str(row[heh_index])])


import csv
import sys
import os
from time import sleep


class BoardIdToAlias:
    boards_filename = ''
    alias_filename = ''
    file_dir = 'caen_logs_formatted'

    def __init__(self):
        self.boards = []
        self.boards_alias = []

    def convert(self, filename):
        # Finding filename for corresponding Alias and log file
        self.boards_filename = filename
        self.alias_filename = 'Aliases/' + filename.replace('.txt', '.csv')

        self.find_board_and_channels()
        self.find_board_aliases()

        # If no values are appended there weren't found any in the log files
        if not (len(self.boards) or len(self.boards_alias)):
            print('No Board IDs or Aliases found..')
            return

        # Create path if not existing
        if not os.path.isdir(self.file_dir):
            os.mkdir(self.file_dir)

        self.rename_txt_file()

    def rename_txt_file(self):
        # Read the entire log file and save it in memory
        with open('caen_logs_unformatted/' + self.boards_filename, 'rb') as r_file:
            filedata = r_file.read()

        # Rename the specified IDs to aliases specified in Aliases files
        for index, boards in enumerate(self.boards):
            msg = 'Renaming ' + boards + ' to ' + self.boards_alias[index]
            print(msg)
            filedata = filedata.replace(str(boards), str(self.boards_alias[index]))

        # Change filename to aliases and save it as csv
        boards_new_filename = self.file_dir + '/' + self.boards_filename
        with open(boards_new_filename, 'w') as w_file:
            w_file.write(filedata)

    def find_board_aliases(self):
        # Read aliases from alias file
        with open(self.alias_filename, 'rb') as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.boards_alias.append(str(row['Alias']))

    def find_board_and_channels(self):
        # Read board and channel IDs from alias file
        with open(self.alias_filename, 'rb') as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.boards.append(str(row['BoardID']))

    # Function to find aliases from the log file (Not necessary when it's specified in alias files)
    def find_board_and_channels_from_file(self):
        max_tries = 1000
        for board in range(0, 10):
            for channel in range(0, 10):
                id_name = 'Board0' + str(board) + '.Chan00' + str(channel)
                with open(self.boards_filename) as in_file:
                    amount_tries = 0
                    for line in in_file:
                        amount_tries += 1
                        if id_name in line:
                            self.boards.append(id_name)
                            break
                        elif amount_tries == max_tries:
                            break

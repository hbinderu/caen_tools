import os
from board_id_to_alias import BoardIdToAlias
from separate_caen_logs import SeparateLogs
from format_radmon import RadmonFormatter
from extract_power_events import ExtractPowerEvents


def check_folders():
    # Checking folders and creating the ones that are needed
    print 'Checking and creating folders....'
    names = ['Aliases', 'caen_logs_formatted', 'caen_logs_unformatted', 'formatLogs', 'gradient', 'TID', 'power_events']
    for items in names:
        if not os.path.isdir(items):
            os.mkdir(items)


def convert_names():
    # Converting names from alias files and saving a new .txt file
    b2a = BoardIdToAlias()
    caen_log_files = os.listdir('caen_logs_unformatted')
    for items in caen_log_files:
        if items in os.listdir('caen_logs_formatted'):
            print items, 'already exists in caen_logs_formatted folder'
            continue
        b2a.convert(items)


def format_radmon(include_unformatted=True):
    radmon_files = os.listdir('TID')
    if not 'radmon.csv' in radmon_files:
        raise OSError('No radmon.csv file was found in dir "TID/"..')

    print '\n'  # Cause prettiness :)

    # CHECK PARAMTERS FOR SETTINGS REGARDING RADMON FILE in .run_format()
    if 'TID.csv' in radmon_files:
        print 'TID.csv already exists in TID folder'
    else:
        print 'Formatting Radmon: TID.csv'
        RadmonFormatter().run_format()

    if include_unformatted and 'TID_unformatted.csv' in radmon_files:
        print 'TID_unformatted.csv already exists in TID folder'
    else:
        print 'Formatting Radmon: TID_unformatted.csv'
        RadmonFormatter().run_unformatted()


def separate_all_logs(initial_currents=True, gradients=True, interpolate=False):   # Change default values if needed
    SL = SeparateLogs()
    print '\n\nSeparate CAEN Logs:\n'
    print 'Finding aliases....'
    SL.find_board_aliases()
    print 'Creating node objects....'
    SL.create_node_objects()

    if initial_currents:
        print 'Assigning initial currents....'
        SL.find_initial_currents()
    if gradients:
        print 'Analysing gradients....'
        SL.find_gradients()

    caen_log_files = os.listdir('caen_logs_formatted')
    for items in caen_log_files:
        print 'Separating: ', items
        SL.separate_txt_file(items, caen_name='2527')   # Change CAEN name to the instance name of caen device

    print 'Extracting TID and HEH....'
    SL.extract_tid(enable_unformatted=False)    # CHECK THIS FUNCTION IF BOARDS ARE TESTED FOR MORE THAN ONE WEEK
    SL.save_all_csv_files(interpolate=interpolate)


def extract_power_events():
    print '\nExtracting Power Events....'
    ExtractPowerEvents()


if __name__ == '__main__':
    check_folders()
    convert_names()
    extract_power_events()
    format_radmon()
    separate_all_logs()

from pyexcelerate import Workbook, Style
import os
import csv


class Nodes:
    def __init__(self, node_id):
        self.__node_id = node_id
        self.__csv_data = []

    def append_csv_data(self, data):
        self.__csv_data.append(data)

    def print_csv_data(self):
        print self.__csv_data

    def get_csv_data(self):
        return self.__csv_data

    def get_node_id(self):
        return self.__node_id


class ExtractPowerEvents:
    status_list = ['ON', 'Ramp Up', 'Ramp Down', 'OverCurrent', 'OverVoltage', 'UnderVoltage', 'External Trip',
                    'Over HVmax', 'External Disable', 'Internal Trip', 'Calibration Error',
                    'Unplugged (remote boards only)', 'UnderCurrent', 'OverVoltage Protection', 'Power Fail',
                   'Temperature Error']

    def __init__(self):
        self.aliases = []
        self.node_ids = []
        self.NodesList = []
        self.size_index = []

        self.log_dir = 'caen_logs_formatted/'
        self.alias_dir = 'Aliases/'
        self.save_file = 'power_events/PowerEvents.xlsx'

        self.wb = Workbook()

        self.setup()
        self.power_events_for_all_nodes()

    def setup(self):
        self.find_board_aliases()
        self.create_nodes()

    def power_events_for_all_nodes(self):
        self.find_pw_and_status()

        for nodes in self.NodesList:
            print 'Write Data to XLSX for node ' + str(nodes.get_node_id())

            # Only add sheet if it contains data
            if len(nodes.get_csv_data()):

                # Extract the long string data into separate lists
                node_id, names, values, timestamps = self.extract_data_lines(nodes.get_csv_data())

                # Converting values of msg from OPC CAEN protocol
                status_pw_msg = self.value_to_status_pw_msg(names, values)

                # Create new sheet, format it and and reset counters
                ws = self.wb.new_sheet(str(node_id))
                ws.set_cell_value(1, 1, 'Analog ID')
                ws.set_cell_value(1, 2, 'Channel Status')
                ws.set_cell_value(1, 3, 'Server Timestamp')
                ws.set_cell_value(1, 5, 'Digital ID')
                ws.set_cell_value(1, 6, 'Channel Status')
                ws.set_cell_value(1, 7, 'Server Timestamp')
                ws.set_cell_value(1, 9, 'Can ID')
                ws.set_cell_value(1, 10, 'Channel Status')
                ws.set_cell_value(1, 11, 'Server Timestamp')

                # We start at second row, as headers are set
                analog_count = 2
                digital_count = 2
                can_count = 2

                # Working with pyexcelerate is not really convenient .. we do everything manually - but it's fast!
                for index in range(len(names)):
                    if 'ANALOG' in names[index]:
                        ws.set_cell_value(analog_count, 1, names[index])
                        ws.set_cell_value(analog_count, 2, status_pw_msg[index])
                        ws.set_cell_value(analog_count, 3, timestamps[index])
                        analog_count += 1
                    elif 'DIGITAL' in names[index]:
                        ws.set_cell_value(digital_count, 5, names[index])
                        ws.set_cell_value(digital_count, 6, status_pw_msg[index])
                        ws.set_cell_value(digital_count, 7, timestamps[index])
                        digital_count += 1
                    elif 'CAN' in names[index]:
                        ws.set_cell_value(can_count, 9, names[index])
                        ws.set_cell_value(can_count, 10, status_pw_msg[index])
                        ws.set_cell_value(can_count, 11, timestamps[index])
                        can_count += 1

                for index in range(1, ws.num_columns + 1):
                    ws.set_col_style(index, Style(size=-1))
        self.wb.save(self.save_file)
        print('Saved as: ' + str(self.save_file))

    def value_to_status_pw_msg(self, names, values):
        msg = []
        for index in range(len(names)):
            if "Status" in names[index]:
                value_msg = []
                # We check with bitmasking 16 bit values
                for index_pow in range(0, 16):
                    # Convert values to bin mask
                    flag = pow(2, index_pow)

                    # Check if bit is set and add msg to list if it is
                    if values[index] & flag:
                        value_msg.append(self.status_list[index_pow])

                    # If first value is not matched, it means that bit 0 = 0 => OFF
                    if not len(value_msg):
                        value_msg.append('OFF')

                # Add the different statues to the msg
                msg.append(str(value_msg))

            if "Pw" in names[index]:
                # We check for value 0/1 and assign correct label
                if values[index]:
                    msg.append('On')
                else:
                    msg.append('Off')

        # Finally return full msg, same order as values (attr)
        return msg

    def extract_data_lines(self, data_lines):
        node_id = self.extract_node_id(data_lines[0])

        name = []
        timestamp = []
        value = []

        for lines in data_lines:
            # Finding the values from string index
            name.append(str(lines[lines.find('2527.')+5 : lines.find('value=')]))
            value.append(int(lines[-77:-72]))
            timestamp.append(str(lines[-25:-2]))

        return node_id, name, value, timestamp

    def find_board_aliases(self):
        alias_files = os.listdir(self.alias_dir)
        msg = 'Files in Alias folder: '
        for aliases in alias_files:
            msg += str(aliases) + ' '
            with open(self.alias_dir + aliases, 'rb') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    alias = str(row['Alias'])
                    # Check for empty rows and sorts them out
                    if alias and not alias == 'TID':
                        self.aliases.append(alias)

        print msg
        # Check for duplicates and sort the rows
        self.aliases = list(set(self.aliases))

    def create_nodes(self):
        for alias in self.aliases:
            node_id = self.extract_node_id(alias)
            self.node_ids.append(node_id)

        # Remove duplicates and sort node ID's from low to high number
        self.node_ids = list(set(self.node_ids))
        self.node_ids.sort()

        # Create objects containing node IDs. Same index as self.node_ids
        for node in self.node_ids:
            NodeObj = Nodes(node)
            self.NodesList.append(NodeObj)

    def extract_node_id(self, alias):
        # We find the value between the '_'s and add them to the list
        node_id = alias[alias.find('_') + 1: alias.find('_') + 3]
        if node_id.isdigit() and node_id:
            return int(node_id)
        else:
            return 0

    def alias_to_index(self, alias):
        return self.aliases.index(alias)

    def node_id_to_index(self, node_id):
        return self.node_ids.index(node_id)

    def find_pw_and_status(self):
        log_files = os.listdir(self.log_dir)

        msg = 'Files in Log folder: '
        for files in log_files:
            msg += str(files) + ' '

        print msg

        for log_file in log_files:
            print 'Analyzing log file: ' + log_file
            with open(self.log_dir + log_file) as in_file:
                for line in in_file:
                    if ('Pw' in line or 'Status' in line) and len(line) > 150:
                        node_id = self.extract_node_id(line)
                        if node_id:
                            node_index = self.node_id_to_index(node_id)
                            self.NodesList[node_index].append_csv_data(line)

    def find_pw_and_status2(self, alias):
        data_lines = []
        with open(self.log_dir + 'caen_log_week3.txt') as in_file:
            for line in in_file:
                if ('Pw' in line or 'Status' in line) and str(alias) in line and len(line) == 156 + len(alias):
                    data_lines.append(str(line))
        return data_lines

    def gen_size_index(self):
        for alias in self.aliases:
            size = len(alias)
            self.size_index.append(size)
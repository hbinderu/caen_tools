import os
import csv
from datetime import datetime
import pandas as pd

from pandas import read_csv as panda_csv
from pandas import datetime as panda_date

import numpy as np


class Nodes:
    def __init__(self, alias):
        self._alias = alias
        self.__csv_data = []
        self.__csv_timestamps = []
        self.__csv_tid = None
        self.__sampled_data = None
        self.tid_gradient = 1
        self.heh_gradient = 1

        self.__initial_current = 0

    def get_alias(self):
        return self._alias

    def set_initial_current(self, current):
        self.__initial_current = current

    def append_csv_data(self, data):
        self.__csv_data.append(data)

    def append_csv_timestamp(self, data):
        self.__csv_timestamps.append(data)

    def set_csv_tid(self, data):
        self.__csv_tid = data

    def print_csv_data(self):
        print self.__csv_data

    def adjust_currents(self):
        if not self.__initial_current:
            return

        # Adjusting the initial currents:
        # Comparing the first value to the initial value and find the offset
        first_measurement = self.__csv_data[0]
        current_offset = first_measurement - self.__initial_current

        tmp_data = []
        tmp_timestamp = []

        # Apply the offset to the data
        for index in range(len(self.__csv_data)):
            adjusted_current = self.__csv_data[index] - current_offset
            if adjusted_current >= 0:
                tmp_data.append(adjusted_current)
                tmp_timestamp.append(self.__csv_timestamps[index])

        self.__csv_data = tmp_data
        self.__csv_timestamps = tmp_timestamp

    def resample_data(self, interpolate, noise=False):
        if self.__csv_data:
            # Upsample plot series with 5S frequency
            sample_series = pd.Series(self.__csv_data, self.__csv_timestamps, name='Currents')

            # Sort the series by index - in case that log files were not in correct order
            sample_series.sort_index(inplace=True)

            # Method to add synthetic noise to the signal - in case that there was missing data and interpolation
            # Looks a little too clean...
            if noise:
                # Generate noise from string_start to string_end defined by timestamp
                timestamp_string_start = '2018-08-01 08:00:00'
                timestamp_string_end = '2018-08-02 17:00:00'

                # Convert string to datetime object
                timestamp = pd.to_datetime(sample_series.index.values[0])
                limit_ts_start = pd.to_datetime(timestamp_string_start)
                limit_ts_end = pd.to_datetime(timestamp_string_end)

                # To check if the values have been assigned after the algorithm
                start_index = None
                end_index = None

                # Specify this interval: here it's from start till given timestamp
                if limit_ts_start < timestamp < limit_ts_end:
                    ts_series = pd.to_datetime(sample_series.index.values)
                    for i, values in enumerate(ts_series):
                        # Find the lowest index where the timestamp is lower than the specified start time
                        if values < limit_ts_start:
                            start_index = i

                        # Find the lowest index where the timestamp is higher than the specified end time
                        if values > limit_ts_end:
                            end_index = i
                            break   # Indexing is now done

                    # If the index doesn't contain a value, raise a value error
                    if not (start_index or end_index):
                        raise ValueError('Start or end index was not generated properly during noise generation')

                    # Index for resampling is found and the signal will now be interpolated during the interval
                    upsampled_noise = sample_series[start_index:end_index].resample('5S').last()
                    interpolated = upsampled_noise.interpolate('pchip')

                    # Generating noise
                    for i, items in enumerate(upsampled_noise):
                        if np.isnan(items):
                            noise = np.random.randint(-1, 2)
                            interpolated[i] += noise
                        else:
                            pass

                    # Adding the clean upsampled signal to the list
                    upsampled_last = sample_series[end_index:].resample('5S').bfill()
                    upsampled_first = sample_series[0:start_index].resample('5S').bfill()

                    signal = upsampled_first.append(interpolated)
                    signal = signal.append(upsampled_last)
                else:
                    signal = sample_series.resample('5S').bfill()
            else:
                if interpolate:
                    signal = sample_series.resample('5S').bfill()
                else:
                    signal = sample_series.resample('5S').last()

            # Combine the two resampled series
            combined = pd.concat([self.__csv_tid['TID'] * self.tid_gradient, self.__csv_tid['HEH'] * self.heh_gradient,
                                  signal], axis=1, names=['TID', 'HEH', 'Currents'])

            # Remove any NaN values
            self.__sampled_data = combined.dropna()

            # If there was current data from before the TID reset, we remove them manually, as we don't care about
            # this data.
            if not self.__sampled_data['TID'][0] == 0:
                zero_index = 0
                for index in range(len(self.__sampled_data['TID'])):
                    if self.__sampled_data['TID'][index] > 1:
                        zero_index += 1
                    else:
                        if self.__sampled_data['TID'][index] == 0:
                            self.__sampled_data = self.__sampled_data[zero_index:]
                        else:
                            self.__sampled_data = self.__sampled_data[zero_index - 1:]
                            self.__sampled_data['TID'][0] = 0
                            self.__sampled_data['HEH'][0] = 0
                        break

    def save_csv_data(self, save_dir, interpolate):
        if self.__csv_data:     # Added for compatibility with multiple alias files but only single log files
            self.adjust_currents()
            self.resample_data(interpolate)
            self.__sampled_data.to_csv(save_dir + str(self._alias) + '.csv')


class SeparateLogs:
    def __init__(self):
        self.alias_dir = 'Aliases/'
        self.save_dir = 'formatLogs/'

        self.board_alias = []
        self.BoardObjs = []

    def create_node_objects(self):
        # Generate objects from the alias files
        for boards in self.board_alias:
            boardObj = Nodes(boards)
            self.BoardObjs.append(boardObj)

    def save_all_csv_files(self, interpolate):
        # Create path if it doesn't exist already
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)

        # Function to save all boards' data as CSV
        for items in self.BoardObjs:
            print 'Saving .csv data for ', items.get_alias()
            items.save_csv_data(self.save_dir, interpolate)

    def find_board_aliases(self):
        # Generates a list to identify the boards aliases from the alias files.
        alias_files = os.listdir(self.alias_dir)
        boards = []
        for aliases in alias_files:
            with open(self.alias_dir + aliases, 'rb') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    boards.append(str(row['Alias']))
        # Creates a list with the alias names
        self.board_alias = list(set(boards))

        # Remove empty aliases
        self.board_alias.remove('')

    def find_gradients(self):
        # In case of flux gradient during radiation test
        file = 'gradient/gradient.csv'
        gradients = []

        # Open the file and append the gradients to a list
        with open(file, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                gradients.append(row)

        # Add the gradient to the board objects
        for boards in self.BoardObjs:
            alias = str(boards.get_alias()).strip()
            nodeid = alias[alias.find('_')+1: alias.find('_')+3]
            for row in gradients:
                row_nodeid = row[0][row[0].find('_')+1: row[0].find('_')+3]
                if nodeid == row_nodeid:
                    boards.tid_gradient = float(row[1])
                    boards.heh_gradient = float(row[2])

    def separate_txt_file(self, filename, caen_name='2527'):
        # Function to separate the txt file to each board objects

        caen_name = caen_name + '.'
        caen_name_len = len(caen_name)

        with open('caen_logs_formatted/' + filename, 'rb') as in_file:
            for line in in_file:
                # Finding IMon values (Monitored current)
                if 'IMon' in line and len(line) > 100:
                    # Extracting the value between 2526.xxxxxxx.IMon (the alias) - placeholder is called 2527
                    alias = line[line.find(caen_name) + caen_name_len : line.find('.IMon')]
                    # Extracting the IMon value, as it's before the ts_source
                    imon_value = float(line[line.find('ts_source') - 8: line.find('ts_source') - 1]) * 1000
                    # Extracting timestamp

                    # windows is different from linux regarding csv-reading from end of string
                    if os.name == 'nt':
                        low_ts = -26
                        high_ts = -3
                    else:
                        low_ts = -25
                        high_ts = -2

                    timestamp = line[low_ts:high_ts]
                    # Convert timestamp to datetime object
                    date = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')

                    # Add data to corresponding Node Object
                    if alias:
                        index = self.alias_to_index(alias)
                        self.BoardObjs[index].append_csv_data(imon_value)
                        self.BoardObjs[index].append_csv_timestamp(date)

    # Add information from TID files
    def extract_tid(self, enable_unformatted=False):
        formatted_tid = self.resample_TID('TID/TID.csv')

        for boards in self.BoardObjs:
            board_id = boards.get_alias()

            # BOARDS TESTED FOR MORE THAN ONE WEEK (PLACE HOLDERS)
            if enable_unformatted and ('41' in board_id or '42' in board_id or '53' in board_id or '61' in board_id):
                unformatted_tid = self.resample_TID('TID/TID_unformatted.csv')
                boards.set_csv_tid(unformatted_tid)
            else:
                boards.set_csv_tid(formatted_tid)

    def resample_TID(self, file):
        # Open TID file as Pandas Series
        series = panda_csv(file, header=0, parse_dates=[0], index_col=0, squeeze=True, date_parser=self.timestamp_parser)

        # Resample and interpolate every5 seconds
        upsampled = series.resample('5S').bfill()
        return upsampled

    def timestamp_parser(self, x):
        return panda_date.strptime(x, '%d/%m/%Y %H:%M:%S')

    # HARD CODED VALUES WILL DIFFER FROM TESTS
    def find_initial_currents(self):
        for boards in self.BoardObjs:
            alias = boards.get_alias()
            if 'ELMB128' in alias:
                if 'ANALOG' in alias:
                    boards.set_initial_current(6)
                elif 'DIGITAL' in alias:
                    boards.set_initial_current(10)
                elif 'CAN' in alias:
                    boards.set_initial_current(20)

            elif 'ELMB2' in alias or 'AT90' in alias:
                if 'direct' in alias:
                    if 'ANALOG' in alias:
                        boards.set_initial_current(8)
                    elif 'DIGITAL' in alias:
                        boards.set_initial_current(8)
                    elif 'CAN' in alias:
                        boards.set_initial_current(12)
                else:
                    if 'ANALOG' in alias:
                        boards.set_initial_current(12)
                    elif 'DIGITAL' in alias:
                        boards.set_initial_current(8)
                    elif 'CAN' in alias:
                        boards.set_initial_current(16)

    def alias_to_index(self, alias):
        return self.board_alias.index(alias)